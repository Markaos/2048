#ifndef NTIMER
#include <time.h>

#include "timer.h"

void timer_init (struct timer *timer) {
    for (int i = 0; i < 32; i++) {
        timer->usecs[i] = 0;
        timer->calls[i] = 0;
        timer->_pending[i] = 0;
        timer->_pending_secs[i] = 0;
    }
}

void timer_section_start (struct timer *timer, int sid) {
    struct timespec spec;
    clock_gettime (CLOCK_MONOTONIC_RAW, &spec);
    timer->_pending[sid] = spec.tv_nsec;
    timer->_pending_secs[sid] = spec.tv_sec;
}

void timer_section_end (struct timer *timer, int sid) {
    struct timespec spec;
    clock_gettime (CLOCK_MONOTONIC_RAW, &spec);
    timer->usecs[sid] += 1000000000 * (spec.tv_sec - timer->_pending_secs[sid]);
    timer->usecs[sid] += (spec.tv_nsec - timer->_pending[sid]);
    timer->calls[sid] += 1;
    timer->_pending[sid] = 0;
    timer->_pending_secs[sid] = 0;
}

long timer_get_nanos (struct timer *timer, int sid) {
    return timer->usecs[sid];
}

long timer_get_calls (struct timer *timer, int sid) {
    return timer->calls[sid];
}

void timer_add (struct timer *dest, struct timer *src) {
    for (int i = 0; i < 16; i++) {
        dest->calls[i] += src->calls[i];
        dest->usecs[i] += src->usecs[i];
    }
}

#endif