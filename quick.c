#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "types.h"
#include "logic.h"

void print_field(playfield *field) {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            if (field->tiles[row][col] == NULL) {
                fputs("      ;", stdout);
            } else {
                assert (field->tiles[row][col] != NULL);
                printf ("%6d;", 1 << field->tiles[row][col]->value);
            }
        }
        puts ("");
    }

    putc ('\n', stdout);
    putc ('\n', stdout);
}

void move_wrapper (playfield *field, int direction) {
    if (direction > 8) return;

    if (!move (field, direction % 4)) {
        move_wrapper (field, direction + 1);
    }
}

int main (int argc, char *argv[]) {
    srand (time (NULL));
    playfield *field = playfield_create ();
    assert (field != NULL);

    while (check_state (field)) {
        spawn (field);
        int dir = rand () % 4;
        move_wrapper (field, dir);
    }

    print_field (field);
    playfield_destroy (field);
}