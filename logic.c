#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "types.h"
#include "logic.h"
#include "qrand.h"

bool move_vertical (playfield *field, int direction);
bool move_horizontal (playfield *field, int direction);
bool move_row_up (playfield *field, int row, int *offsets);
bool move_row_down (playfield *field, int row, int *offsets);
bool move_col_left (playfield *field, int col, int *offsets);
bool move_col_right (playfield *field, int col, int *offsets);
void logic_section_start (playfield *field, int section);
void logic_section_end (playfield *field, int section);
void logic_destroy_tile (playfield *field, tile *t);
tile *logic_alloc_tile (playfield *field);

playfield *playfield_create () {
    playfield *field = (playfield *) malloc(sizeof(playfield));
    for(int i = 0; i < 16; i++) {
        field->tiles[i / 4][i % 4] = NULL;
    }
    field->timer = NULL;
    field->clean = true;
    field->has_empty_tiles = true;
    field->memory = malloc (sizeof(tile) * 16 + sizeof(int));
    memset (field->memory + 16 * sizeof(tile), 0, sizeof(int));

    spawn (field);
    spawn (field);

    return field;
}

playfield *playfield_create_rd (int *rd) {
    playfield *field = (playfield *) malloc(sizeof(playfield));
    for(int i = 0; i < 16; i++) {
        field->tiles[i / 4][i % 4] = NULL;
    }
    field->timer = NULL;
    field->clean = true;
    field->has_empty_tiles = true;
    field->memory = malloc (sizeof(tile) * 16 + sizeof(int));
    memset (field->memory + 16 * sizeof(tile), 0, sizeof(int));

    spawn_rd (field, rd);
    spawn_rd (field, rd);

    return field;
}

playfield *playfield_create_rd_pre (int *rd, void *memory) {
    playfield *field = (playfield *) memory;
    for(int i = 0; i < 16; i++) {
        field->tiles[i / 4][i % 4] = NULL;
    }
    field->timer = NULL;
    field->clean = true;
    field->has_empty_tiles = true;
    field->memory = memory + sizeof(playfield);
    memset (field->memory + 16 * sizeof(tile), 0, sizeof(int));

    spawn_rd (field, rd);
    spawn_rd (field, rd);

    return field;
}

void playfield_destroy (playfield *field) {
    free (field->memory);
    free (field);
}

void spawn (playfield *field) {
    logic_section_start (field, LOGIC_SECTION_SPAWN);
    if(check_state (field)) {
        int pos = rand() % 16;
        if (field->tiles[pos / 4][pos % 4] == NULL) {
            tile *t = logic_alloc_tile (field);
            t->value = rand() % 10 == 0 ? 2 : 1;
            field->tiles[pos / 4][pos % 4] = t;
            field->clean = false;
        } else {
            logic_section_end (field, LOGIC_SECTION_SPAWN);
            spawn(field);
            return;
        }
    }
    logic_section_end (field, LOGIC_SECTION_SPAWN);
}

void spawn_rd (playfield *field, int *rd) {
    logic_section_start (field, LOGIC_SECTION_SPAWN);
    if(check_state (field)) {
        int pos = fast_rand (rd) % 16;
        if (field->tiles[pos / 4][pos % 4] == NULL) {
            tile *t = logic_alloc_tile (field);
            t->value = fast_rand (rd) % 10 == 0 ? 2 : 1;
            field->tiles[pos / 4][pos % 4] = t;
            field->clean = false;
        } else {
            logic_section_end (field, LOGIC_SECTION_SPAWN);
            spawn_rd (field, rd);
            return;
        }
    }
    logic_section_end (field, LOGIC_SECTION_SPAWN);
}

bool check_state (playfield *field) {
    logic_section_start (field, LOGIC_SECTION_CHECK_STATE);
    if(field->clean) {
        return field->has_empty_tiles;
    }

    for(int i = 0; i < 16; i++) {
        if(field->tiles[i / 4][i % 4] == NULL) {
            field->clean = true;
            field->has_empty_tiles = true;
            logic_section_end (field, LOGIC_SECTION_CHECK_STATE);
            return true;
        }
    }

    field->clean = true;
    field->has_empty_tiles = false;
    logic_section_end (field, LOGIC_SECTION_CHECK_STATE);
    return false;
}

int largest_value_unsafe (playfield *field) {
    logic_section_start (field, LOGIC_SECTION_VALUE);
    assert (!check_state (field));
    int l = 0;
    for(int i = 0; i < 16; i++) {
        if(field->tiles[i / 4][i % 4] != NULL) {
            if (field->tiles[i / 4][i % 4]->value > l) {
                l = field->tiles[i / 4][i % 4]->value;
            }
        }
    }
    logic_section_end (field, LOGIC_SECTION_VALUE);
    return l;
}

bool move (playfield *field, int direction) {
    logic_section_start (field, LOGIC_SECTION_MOVE);
    bool rVal;
    switch (direction) {
        default:
        case 0: rVal = move_vertical (field, direction); break;
        case 1: rVal = move_vertical (field, direction); break;
        case 2: rVal = move_horizontal (field, direction); break;
        case 3: rVal = move_horizontal (field, direction); break;
    }
    if (rVal == true) {
        field->clean = true;
        field->has_empty_tiles = true;
    }
    logic_section_end (field, LOGIC_SECTION_MOVE);
    return rVal;
}

void set_timer (playfield *field, struct timer *timer) {
    field->timer = timer;
}

bool move_vertical (playfield *field, int direction) {
    bool moved = false;
    int fRow = direction == 0 ? 0 : 3;

    int offsets[] = {0, 0, 0, 0};

    for (int col = 0; col < 4; col++) {
        if (field->tiles[fRow][col] != NULL) {
            offsets[col] = 1;
        }
    }
    if (direction == 0) {
        for (int row = 1; row < 4; row++) {
            moved |= move_row_up (field, row, offsets);
        }
    } else {
        for (int row = 2; row >= 0; row--) {
            moved |= move_row_down (field, row, offsets);
        }
    }

    return moved;
}

bool move_horizontal (playfield *field, int direction) {
    bool moved = false;
    int fCol = direction == 2 ? 0 : 3;

    int offsets[] = {0, 0, 0, 0};

    for (int row = 0; row < 4; row++) {
        if (field->tiles[row][fCol] != NULL) {
            offsets[row] = 1;
        }
    }

    if (direction == 2) {
        for (int col = 1; col < 4; col++) {
            moved |= move_col_left (field, col, offsets);
        }
    } else {
        for (int col = 2; col >= 0; col--) {
            moved |= move_col_right (field, col, offsets);
        }
    }

    return moved;
}

bool move_row_up (playfield *field, int row, int *offsets) {
    bool moved = false;

    for (int col = 0; col < 4; col++) {
        if (field->tiles[row][col] == NULL) continue;
        
        if (offsets[col] - 1 != row && offsets[col] > 0 && field->tiles[row][col]->value == field->tiles[offsets[col] - 1][col]->value) {
            assert (field->tiles[offsets[col] - 1][col] != NULL);
            assert (field->tiles[offsets[col] - 1][col] != field->tiles[row][col]);

            logic_destroy_tile (field, field->tiles[row][col]);
            field->tiles[row][col] = NULL;
            field->tiles[offsets[col] - 1][col]->value++;
            moved = true;
        } else {
            if (offsets[col] != row) {
                assert (field->tiles[offsets[col]][col] == NULL);

                field->tiles[offsets[col]][col] = field->tiles[row][col];
                field->tiles[row][col] = NULL;
                moved = true;
            }

            offsets[col]++;
        }
    }

    return moved;
}

bool move_row_down (playfield *field, int row, int *offsets) {
    bool moved = false;

    for (int col = 0; col < 4; col++) {
        if (field->tiles[row][col] == NULL) continue;

        if (4 - offsets[col] != row && offsets[col] > 0 && field->tiles[row][col]->value == field->tiles[4 - offsets[col]][col]->value) {
            assert (field->tiles[4 - offsets[col]][col] != NULL);
            assert (field->tiles[4 - offsets[col]][col] != field->tiles[row][col]);

            logic_destroy_tile (field, field->tiles[row][col]);
            field->tiles[row][col] = NULL;
            field->tiles[4 - offsets[col]][col]->value++;
            moved = true;
        } else {
            if (3 - offsets[col] != row) {
                assert (field->tiles[3 - offsets[col]][col] == NULL);

                field->tiles[3 - offsets[col]][col] = field->tiles[row][col];
                field->tiles[row][col] = NULL;
                moved = true;
            }

            offsets[col]++;
        }
    }

    return moved;
}

bool move_col_left (playfield *field, int col, int *offsets) {
    bool moved = false;

    for (int row = 0; row < 4; row++) {
        if (field->tiles[row][col] == NULL) continue;
        
        if (offsets[row] - 1 != col && offsets[row] > 0 && field->tiles[row][col]->value == field->tiles[row][offsets[row] - 1]->value) {
            assert (field->tiles[row][offsets[row] - 1] != NULL);
            assert (field->tiles[row][offsets[row] - 1] != field->tiles[row][col]);

            logic_destroy_tile (field, field->tiles[row][col]);
            field->tiles[row][col] = NULL;
            field->tiles[row][offsets[row] - 1]->value++;
            moved = true;
        } else {
            if (offsets[row] != col) {
                if (field->tiles[row][offsets[row]] != NULL) {
                    putc(*(int *) NULL, stdout);
                }
                assert (field->tiles[row][offsets[row]] == NULL);

                field->tiles[row][offsets[row]] = field->tiles[row][col];
                field->tiles[row][col] = NULL;
                moved = true;
            }

            offsets[row]++;
        }
    }

    return moved;
}

bool move_col_right (playfield *field, int col, int *offsets) {
    bool moved = false;

    for (int row = 0; row < 4; row++) {
        if (field->tiles[row][col] == NULL) continue;

        if (4 - offsets[row] != col && offsets[row] > 0 && field->tiles[row][col]->value == field->tiles[row][4 - offsets[row]]->value) {
            assert (field->tiles[row][4 - offsets[row]] != NULL);
            assert (field->tiles[row][4 - offsets[row]] != field->tiles[row][col]);

            logic_destroy_tile (field, field->tiles[row][col]);
            field->tiles[row][col] = NULL;
            field->tiles[row][4 - offsets[row]]->value++;
            moved = true;
        } else {
            if (3 - offsets[row] != col) {
                assert (field->tiles[row][3 - offsets[row]] == NULL);

                field->tiles[row][3 - offsets[row]] = field->tiles[row][col];
                field->tiles[row][col] = NULL;
                moved = true;
            }

            offsets[row]++;
        }
    }

    return moved;
}

void logic_section_start (playfield *field, int section) {
    if (field->timer != NULL) {
        timer_section_start (field->timer, section);
    }
}

void logic_section_end (playfield *field, int section) {
    if (field->timer != NULL) {
        timer_section_end (field->timer, section);
    }
}

inline static bool used (short array, int index) {
    return (array & (1 << index));
}

tile *logic_alloc_tile (playfield *field) {
    logic_section_start (field, LOGIC_SECTION_TILE_ALLOC);
    short *tile_count = (short *)(field->memory + 16 * sizeof(tile));
    short *array = (short *)(field->memory + 16 * sizeof(tile) + sizeof(short));
    assert (*tile_count < 16);

    int index = -1;
    for (int i = 0; i < 16; i++) {
        if (!used (*array, i)) {
            index = i;
            break;
        }
    }

    assert (index > -1);
    assert (index < 16);

    *array |= 1 << index;
    tile *t = (tile *) (field->memory + index * sizeof(tile));

    assert (((void *) t - field->memory) / sizeof (tile) < 16);
    *tile_count += 1;
    logic_section_end (field, LOGIC_SECTION_TILE_ALLOC);
    return t;
}

void logic_destroy_tile (playfield *field, tile *t) {
    logic_section_start (field, LOGIC_SECTION_TILE_DESTROY);
    short *tile_count = (short *)(field->memory + 16 * sizeof(tile));
    short *array = (short *)(field->memory + 16 * sizeof(tile) + sizeof(short));
    int index = ((void *) t - (void *) field->memory) / sizeof(tile);
    *array &= ~(1 << index);
    *tile_count -= 1;
    logic_section_end (field, LOGIC_SECTION_TILE_DESTROY);
}