cmake_minimum_required( VERSION 3.0 )

project ( 2048 )
include_directories( . )

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Disable debug
add_definitions (-DNDEBUG)

# Disable timer
add_definitions (-DNTIMER)

# Optimizations
add_definitions (-O3)

add_library(timer timer.c)
add_library(logic logic.c)

target_link_libraries(logic timer)

add_executable(test_random random.c)
target_link_libraries(test_random logic)

add_executable(test_quick_random quick.c)
target_link_libraries(test_quick_random logic)

add_executable(test_uquick uquick.c)
target_link_libraries(test_uquick logic)

add_executable(test_tuquick tuquick.c)
target_link_libraries(test_tuquick logic timer pthread)