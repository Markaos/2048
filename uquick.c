#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "types.h"
#include "logic.h"
#include "qrand.h"

void print_field(playfield *field) {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            if (field->tiles[row][col] == NULL) {
                fputs("      ;", stdout);
            } else {
                assert (field->tiles[row][col] != NULL);
                printf ("%6d;", 1 << field->tiles[row][col]->value);
            }
        }
        puts ("");
    }

    putc ('\n', stdout);
    putc ('\n', stdout);
}

void move_wrapper (playfield *field, int direction) {
    if (direction > 8) return;

    if (!move (field, direction % 4)) {
        move_wrapper (field, direction + 1);
    }
}

int main (int argc, char *argv[]) {
    int repeats = 100000;
    if(argc > 1) {
        repeats = atoi(argv[1]);
    }

    srand (time (NULL));
    int rd = fast_srand (rand ());
    int counts[32];
    for (int i = 0; i < 32; i++) {
        counts[i] = 0;
    }

    printf("Making %d samples...\n", repeats);
    
    for (int i = 0; i < repeats; i++) {
        playfield *field = playfield_create_rd (&rd);
        assert (field != NULL);

        while (check_state (field)) {
            spawn_rd (field, &rd);
            int dir = fast_rand (&rd) % 4;
            move_wrapper (field, dir);
        }

        counts[largest_value_unsafe (field)]++;
        playfield_destroy (field);
    }

    for (int i = 0; i < 32; i++) {
        printf("%10u: %d (%g)\n", 1 << i, counts[i], counts[i] / (double) repeats);
    }
}