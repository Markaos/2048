#pragma once
#include <stdbool.h>

#include "timer.h"

struct tile {
    int value;
    struct {
        int col;
        int row;
    } pos;
};

struct playfield {
    struct tile *tiles[4][4];
    bool clean;
    bool has_empty_tiles;
    struct timer *timer;
    void *memory;
};

typedef struct tile tile;
typedef struct playfield playfield;