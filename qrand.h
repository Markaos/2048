#pragma once

inline static int fast_srand(int seed) {
    return seed;
}

inline static int fast_rand(int *seed) {
    *seed = (214013*(*seed)+2531011);
    return (*seed>>16)&0x7FFF;
}