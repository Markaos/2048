#pragma once

#include <time.h>

struct timer {
    long usecs[32];
    long calls[32];
    long _pending[32];
    time_t _pending_secs[32];
};

#ifndef NTIMER

void timer_init (struct timer *timer);
void timer_section_start (struct timer *timer, int sid);
void timer_section_end (struct timer *timer, int sid);
long timer_get_nanos (struct timer *timer, int sid);
long timer_get_calls (struct timer *timer, int sid);
void timer_add (struct timer *dest, struct timer *src);

#else

#define timer_init(x) (false)
#define timer_section_start(x, y) (false)
#define timer_section_end(x, y) (false)
#define timer_get_nanos(x, y) ((unsigned long) 0) 
#define timer_get_calls(x, y) ((unsigned long) 0) 
#define timer_add(x, y) (false) 

#endif