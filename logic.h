#pragma once
#include "types.h"

#define LOGIC_SECTION_CREATE 2
#define LOGIC_SECTION_SPAWN 3
#define LOGIC_SECTION_MOVE 4
#define LOGIC_SECTION_CHECK_STATE 5
#define LOGIC_SECTION_VALUE 6
#define LOGIC_SECTION_TILE_ALLOC 7
#define LOGIC_SECTION_TILE_DESTROY 8

#define LOGIC_MEMORY_REQ sizeof(playfield) + 16 * sizeof(tile) + 2 * sizeof(short)

// Create a playfield
playfield *playfield_create ();

// Create a playfield using fast_rand ()
playfield *playfield_create_rd (int *rd);

// Create a playfield using preallocated memory
playfield *playfield_create_rd_pre (int *rd, void *memory);

// Free all resources
void playfield_destroy (playfield *field);

// Move in the specified direction
// 0: Up
// 1: Down
// 2: Left
// 3: Right
bool move (playfield *field, int direction);

// Spawn a new tile
void spawn (playfield *field);

// Spawn a new tile, use fast_rand ()
void spawn_rd (playfield *field, int *rd);

// Check if there are any empty tiles in the playfield
bool check_state (playfield *field);

// Get largest value on the playfield
// Can only run on finished playfields
int largest_value_unsafe (playfield *field);

void set_timer (playfield *field, struct timer *timer);