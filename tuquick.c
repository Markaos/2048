#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/sysinfo.h>

#include "types.h"
#include "logic.h"
#include "qrand.h"
#include "timer.h"

#define SECTION_CRITICAL 0
#define SECTION_COMPUTE 1

struct work_queue {
    pthread_mutex_t *lock;
    pthread_mutex_t *c_lock;
    pthread_cond_t *cond;
    int chunk_size;
    int total_remaining;
    int counts[32];
    struct timer timer;
};

void move_wrapper (playfield *field, int direction) {
    if (direction > 8) return;

    if (!move (field, direction % 4)) {
        move_wrapper (field, direction + 1);
    }
}

void do_work (int repeats, int *counts, int *rd, struct timer *timer) {
    void *memory = malloc (LOGIC_MEMORY_REQ);
    for (int i = 0; i < repeats; i++) {
        timer_section_start (timer, LOGIC_SECTION_CREATE);
        playfield *field = playfield_create_rd_pre (rd, memory);
        timer_section_end (timer, LOGIC_SECTION_CREATE);
        set_timer (field, timer);
        assert (field != NULL);

        while (check_state (field)) {
            spawn_rd (field, rd);
            int dir = fast_rand (rd) % 4;
            move_wrapper (field, dir);
        }

        counts[largest_value_unsafe (field)]++;
        // playfield_destroy (field);
    }
    free (memory);
}

void *worker_main (void *data) {
    struct work_queue *queue = (struct work_queue *) data;
    int repeats = queue->chunk_size;
    int rd = fast_srand (rand ());

    struct timer timer;
    timer_init (&timer);

    int counts[32];
    bool first = true;

    while (true) {
        // printf ("Locking...\n");
        timer_section_start (&timer, SECTION_CRITICAL);
        pthread_mutex_lock(queue->lock);

        if (!first) {
            for (int i = 0; i < 32; i++) {
                queue->counts[i] += counts[i];
            }
        } else {
            first = false;
        }

        if (queue->total_remaining <= 0) {
            timer_section_end (&timer, SECTION_CRITICAL);
            timer_add (&(queue->timer), &timer);
            pthread_mutex_unlock(queue->lock);
            //pthread_exit(NULL);
            return NULL;
        }
        int remaining = queue->total_remaining;
        queue->total_remaining = remaining - repeats;

        pthread_mutex_unlock (queue->lock);
        timer_section_end (&timer, SECTION_CRITICAL);
        timer_section_start (&timer, SECTION_COMPUTE);

        // printf("Thread %lu: Doing %d samples out of %d remaining...\n", pthread_self(), repeats, remaining);

        for (int i = 0; i < 32; i++) {
            counts[i] = 0;
        }

        do_work(queue->chunk_size, counts, &rd, &timer);
        // pthread_cond_signal (queue->cond);

        timer_section_end (&timer, SECTION_COMPUTE);
    }
}

int main (int argc, char *argv[]) {
    int samples = 100000;
    int thread_count = get_nprocs ();
    if(argc > 1) {
        samples = atoi (argv[1]);
        if (argc > 2) {
            thread_count = atoi (argv[2]);
        }
    }

    pthread_t *threads = malloc (sizeof(pthread_t) * thread_count);

    srand (time (NULL));

    printf("Generating %d samples in %d threads...\n", samples, thread_count);
    
    struct work_queue *q = (struct work_queue *) malloc (sizeof(struct work_queue));

    for (int i = 0; i < 32; i++) {
        q->counts[i] = 0;
    }
    q->chunk_size = 1000;
    q->total_remaining = samples;
    q->lock = (pthread_mutex_t *) malloc (sizeof(pthread_mutex_t));
    q->c_lock = (pthread_mutex_t *) malloc (sizeof(pthread_mutex_t));
    q->cond = (pthread_cond_t *) malloc (sizeof(pthread_cond_t));
    pthread_mutex_init (q->lock, NULL);
    pthread_mutex_init (q->c_lock, NULL);
    pthread_cond_init (q->cond, NULL);

    for (int i = 0; i < thread_count; i++) {
        pthread_create (&threads[i], NULL, worker_main, q);
    }

    /*bool quit = false;
    int onscreen_percentage = 0;
    while (!quit) {
        pthread_cond_wait (q->cond, q->c_lock);
        int percentage = (samples - q->total_remaining) * 100 / samples;

        if (q->total_remaining <= 0) quit = true;
    }*/

    for (int i = 0; i < thread_count; i++) {
        pthread_join (threads[i], NULL);
    }
    
    #ifndef NTIMER
    puts ("");
    puts ("Time graph:");
    printf ("  Critical: %lu ns\n", timer_get_nanos (&(q->timer), SECTION_CRITICAL));
    printf ("  --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), SECTION_CRITICAL) / thread_count);
    printf ("  Compute: %lu ns\n", timer_get_nanos (&(q->timer), SECTION_COMPUTE));
    printf ("  --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), SECTION_COMPUTE) / thread_count);
    puts ("  Game logic:");
    printf ("    Create: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_CREATE));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_CREATE) / thread_count);
    printf ("    Spawn: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_SPAWN));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_SPAWN) / thread_count);
    printf ("    State: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_CHECK_STATE));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_CHECK_STATE) / thread_count);
    printf ("    Move: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_MOVE));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_MOVE) / thread_count);
    printf ("    Value: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_VALUE));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_VALUE) / thread_count);
    printf ("    Alloc: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_TILE_ALLOC));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_TILE_ALLOC) / thread_count);
    printf ("    Destroy: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_TILE_DESTROY));
    printf ("    --Per thread: %lu ns\n", timer_get_nanos (&(q->timer), LOGIC_SECTION_TILE_DESTROY) / thread_count);
    puts ("");
    puts ("Call graph:");
    printf ("  Critical: %lu\n", timer_get_calls (&(q->timer), SECTION_CRITICAL));
    printf ("  --Per thread: %lu\n", timer_get_calls (&(q->timer), SECTION_CRITICAL) / thread_count);
    printf ("  Compute: %lu\n", timer_get_calls (&(q->timer), SECTION_COMPUTE));
    printf ("  --Per thread: %lu\n", timer_get_calls (&(q->timer), SECTION_COMPUTE) / thread_count);
    puts ("  Game logic:");
    printf ("    Create: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_CREATE));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_CREATE) / thread_count);
    printf ("    Spawn: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_SPAWN));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_SPAWN) / thread_count);
    printf ("    State: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_CHECK_STATE));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_CHECK_STATE) / thread_count);
    printf ("    Move: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_MOVE));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_MOVE) / thread_count);
    printf ("    Value: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_VALUE));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_VALUE) / thread_count);
    printf ("    Alloc: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_TILE_ALLOC));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_TILE_ALLOC) / thread_count);
    printf ("    Destroy: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_TILE_DESTROY));
    printf ("    --Per thread: %lu\n", timer_get_calls (&(q->timer), LOGIC_SECTION_TILE_DESTROY) / thread_count);
    puts ("");
    #endif

    for (int i = 0; i < 32; i++) {
        printf("%10u: %d (%g)\n", 1 << i, q->counts[i], q->counts[i] / (double) samples);
    }
}